<?php

class Address
{
    private string $line_1;
    private ?string $line_2;
    private string $city;
    private string $state;
    private string $zip;

    public function __construct(string $line_1, ?string $line_2 = null, string $city, string $state, string $zip)
    {
        $this->line_1 = $line_1;
        $this->line_2 = $line_2;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
    }

    public function getLine1(): string
    {
        return $this->line_1;
    }

    public function setLine1(string $line_1): void
    {
        $this->line_1 = $line_1;
    }

    public function getLine2(): ?string
    {
        return $this->line_2;
    }

    public function setLine2(?string $line_2): void
    {
        $this->line_2 = $line_2;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): void
    {
        $this->state = $state;
    }

    public function getZip(): string
    {
        return $this->zip;
    }

    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

}

class Item
{
    private int $id;
    private string $name;
    private int $quantity;
    private float $price;

    public function __construct(int $id, string $name, int $quantity, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
}

class Customer
{
    private string $first_name;
    private string $last_name;
    private array $addresses;

    public function __construct(string $first_name, string $last_name, array $addresses)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->addresses = $addresses;
    }

    public function getFirstName(): string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): void
    {
        $this->first_name = $first_name;
    }

    public function getLastName(): string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }

    public function getAddresses(): array
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): void
    {
        $this->addresses[] = $address;
    }
}

class Cart
{
    private Customer $customer;
    private ?array $items;

    public function __construct(Customer $customer = null)
    {
        $this->customer = $customer;
        $this->items = [];
    }

    public function addItem(Item $item): void
    {
        if ($item) {
            $this->items[] = $item;
        }
    }

    public function getCustomerName(): string
    {
        return $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
    }

    public function getCustomerAddresses(): array
    {
        return $this->customer->getAddresses();
    }

    public function getItems(): array
    {
        return $this->items;
    }

    // Get the shipping address (assuming the first address is the shipping address)
    public function getShippingAddress(): ?Address
    {
        return $this->customer && $this->customer->getAddresses() ? $this->customer->getAddresses()[0] : null;
    }
    public function getFormattedShippingAddress(): string
    {
        $address = $this->getShippingAddress();
        $addressLines[] = $address->getLine1();
        if(strlen($address->getLine2() > 0)){
            $addressLines[] = $address->getLine2();
        }
        $addressLines[] = $address->getCity();
        $addressLines[] = $address->getState();
        $addressLines[] = $address->getZip();
        return implode(' ', $addressLines);
    }
    public function calculateSubtotal(): ?float 
    {
        $subtotal = 0;

            foreach ($this->items as $item) {
                $subtotal += $item->getQuantity() * $item->getPrice();
            }
            return $subtotal;
    }
    public function calculateTotal(): ?float
    {
        if (!$this->items) {
            return null;
        }

        $subtotal = $this->calculateSubtotal();
        // Add tax
        $taxRate = 0.07;
        $tax = $subtotal * $taxRate;

        // Access shipping rate API and add shipping cost
        $shippingCost = $this->getShippingCost();
        $subtotal += $shippingCost;

        // Return the total cost
        return $subtotal + $tax;
    }

    // Access shipping rate API (placeholder method)
    private function getShippingCost(): float
    {
        // For demonstration purposes, returning a static value
        return 10.00;
    }
}

// Setup
$customerAddress = new Address("155 Scarlett Ln", "", "Weirton", "WV", "26062");
$customer = new Customer("Chris", "Reed", [$customerAddress]);
$cart = new Cart($customer);
$item1 = new Item(1, "Product A", 2, 20.00);
$item2 = new Item(2, "Product B", 1, 15.00);
$cart->addItem($item1);
$cart->addItem($item2);

// Display information
echo "Customer Name: " . $cart->getCustomerName() . PHP_EOL;
echo "Shipping Address: ";
echo $cart->getFormattedShippingAddress(). PHP_EOL;
echo "Subtotal: $". number_format($cart->calculateSubtotal(), 2).PHP_EOL;
echo "Total: $" . number_format($cart->calculateTotal(), 2).PHP_EOL;
echo "Items in Cart: ".PHP_EOL;
$cart_items = $cart->getItems();
if(count($cart_items) > 0){
foreach($cart_items as $item)
{
    echo "\t".$item->getName().PHP_EOL;
    echo "\t\t Qty: ".$item->getQuantity().PHP_EOL;
    echo "\t\t Price: $".number_format($item->getPrice(), 2). PHP_EOL;
}
}

