<?php
require_once "arrayData.php";

function printArray(array $array, int $indent = 0): void {
    $spaces = 3; // number of spaces to indent

    // Iterate through each key-value pair in the array
    foreach ($array as $key => $value) {
        // Calculate the indentation based on the current level
        $indentation = str_repeat(" ", $indent * $spaces);

        // Check if the value is an array for recursive printing
        if (is_array($value)) {
            // Print the key with indentation and move to a new line
            echo "$indentation$key =>" . PHP_EOL;
            
            // Recursive call to print the nested array with increased indentation
            printArray($value, $indent + 1);
        } else {
            // Print the key-value pair with indentation
            echo "$indentation$key => $value" . PHP_EOL;
        }
    }
}

// Display array
printArray($array_data);
