<?php
require_once "arrayData.php";

function sortArray(array $data, array $sort_keys): array
{
    // Function to recursively search for the key in nested arrays
    $recursive_search = function ($item, $key) use (&$recursive_search) {
        foreach ($item as $k => $value) {
            // If the current key matches the target key, return the corresponding value
            if ($k === $key) {
                return $value;
            } elseif (is_array($value)) {
                // If the current value is an array, recursively search for the key within the nested array
                $result = $recursive_search($value, $key);
                // If the result is not null, return the result
                if ($result !== null) {
                    return $result;
                }
            }
        }
        // Return null if the key is not found
        return null;
    };

    // Sort the array using a custom comparison function
    usort($data, function ($a, $b) use ($sort_keys, $recursive_search) {
        foreach ($sort_keys as $key) {
            // Retrieve the values associated with the specified keys for the two elements being compared
            $value_a = $recursive_search($a, $key);
            $value_b = $recursive_search($b, $key);

            // Check if the values are different
            if ($value_a != $value_b) {
                // Sort ascending by default
                $order = 1;

                // Perform the actual comparison
                return ($value_a < $value_b) ? -$order : $order;
            }
        }
        // If all specified keys have equal values, return 0 (no change in order)
        return 0;
    });

    // Return the sorted array
    return $data;
}

// Output sorted array
$sortedArray = sortArray($array_data, ['last_name', 'account_id']);
print_r($sortedArray);